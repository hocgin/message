package in.hocg.message.worker.constant;

/**
 * @author hocgin
 */
public interface MQConstant {
    interface Group {
        String TEST_CONSUMER = "TestConsumerGroup";
    }
    
    interface Topic {
        String TEST_TOPIC = "test-topic";
    }
}
