package in.hocg.message.body;

/**
 * Created by hocgin on 2019/3/2.
 * email: hocgin@gmail.com
 *
 * @author hocgin
 */
public interface MessageConstant {
    byte TEST_REQUEST = 1;
    byte TEST_RESPONSE = 2;

}
